'use client'
import Image from "next/image";
import Weather from '../components/Weather';

export default function Home() {
  return (
    <main className="bg-white flex min-h-screen w-screen items-center justify-center p-24 pt-5">
        <Weather/>
    </main>
  );
}