# Weather App
This is a simple weather application built with React.js, Next.js, and Tailwind CSS. The app fetches weather data from the WeatherAPI and displays it to the user.

## Features

- **Weather Data**: Fetches weather data from the WeatherAPI based on the user's input (city name).
- **Dynamic Image Rendering**: Displays weather icons dynamically based on the current weather condition.
- **Responsive Design**: Utilizes Tailwind CSS for responsive and mobile-first design.

## Technologies Used

- **React.js**: A JavaScript library for building user interfaces.
- **Next.js**: A React framework for building server-rendered applications.
- **Tailwind CSS**: A utility-first CSS framework for quickly building custom designs.

## Folder Structure
- **pages/**: Contains Next.js pages.
- **components/**: Contains React components.
- **styles/**: Contains global styles and Tailwind CSS configuration.

## Project status
This project is currently under active development.
